package com.abtracker.server.user;

import com.abtracker.server.domain.Status;
import com.abtracker.server.messages.UserStatus;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;


@Controller
public class UserController {

    private StatusService statusService;

    public UserController(StatusService statusService) {
        this.statusService = statusService;
    }

    @MessageMapping("/status")
    @SendToUser(destinations = "/queue/main", broadcast=false )
    public UserStatus setUserStatus(UserStatus statusMessage) {
        Status status = statusMessage.getStatus();
        if (status != null) {
            statusService.setStatus(status);
        }
        return statusMessage;
    }
}
