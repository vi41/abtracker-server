package com.abtracker.server.user;

import com.abtracker.server.domain.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@Service
public class UserService {

    private UserDal userDal;

    private PasswordEncoder passwordEncoder;

    private TokenStore tokenStore;

    public UserService(UserDal userDal, PasswordEncoder passwordEncoder, TokenStore tokenStore) {
        this.userDal = userDal;
        this.passwordEncoder = passwordEncoder;
        this.tokenStore = tokenStore;
    }

    private String getUserName() {
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }

    @SuppressWarnings("unchecked")
    @ResponseBody
    @RequestMapping(value = "rest/user")
    @Transactional
    public Map<String, Object> getUser() {
        Map<String, Object> res = null;
        User user = userDal.loadUserByName(getUserName());
        if (user != null) {
            ObjectMapper mapper = new ObjectMapper();
            res = mapper.convertValue(user, HashMap.class);
            res.remove("password");
        }
        return res;
    }

    @RequestMapping(value = "rest/logout")
    @ResponseStatus(HttpStatus.OK)
    public void revokeToken(HttpServletRequest request) {
        String authHeader = request.getHeader("Authorization");
        if (authHeader != null) {
            String tokenValue = authHeader.replace("Bearer", "").trim();
            OAuth2AccessToken accessToken = tokenStore.readAccessToken(tokenValue);
            tokenStore.removeAccessToken(accessToken);
            tokenStore.removeRefreshToken(accessToken.getRefreshToken());
        }
    }

    @ResponseBody
    @RequestMapping(value = "rest/register")
    @Transactional
    public ResponseEntity<String> register(@RequestBody User user) {
        String error = null;
        if (user == null) {
            error = "Request doesn't contains user object";
        } else if (StringUtils.isEmpty(user.getName())) {
            error = "Username is empty";
        } else if (StringUtils.isEmpty(user.getFirstName())) {
            error = "First name is empty";
        } else if (StringUtils.isEmpty(user.getLastName())) {
            error = "Last name is empty";
        } else if (StringUtils.isEmpty(user.getPassword())) {
            error = "Password empty";
        } else {
            user.init(passwordEncoder.encode(user.getPassword()));
            userDal.saveUser(user);
        }
        return !StringUtils.isEmpty(error) ? new ResponseEntity<>(error, HttpStatus.BAD_REQUEST) :
                new ResponseEntity<>(HttpStatus.OK);
    }
}
