package com.abtracker.server.user;

import com.abtracker.server.core.AuthUtils;
import com.abtracker.server.core.SessionDataDal;
import com.abtracker.server.domain.Status;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.UUID;

@Service
public class StatusService {

    private SessionDataDal sessionDataDal;

    private AuthUtils authUtils;

    public StatusService(SessionDataDal sessionDataDal, AuthUtils authUtils) {
        this.sessionDataDal = sessionDataDal;
        this.authUtils = authUtils;
    }

    @PostConstruct
    public void init() {
        sessionDataDal.resetAllStatusesInSeparateSession();
    }

    @Transactional
    public void setStatus(Status status) {
        setStatus(authUtils.getUserId(), status);
    }

    @Transactional
    public void setStatus(UUID userId, Status status) {
        sessionDataDal.setStatus(userId, status);
    }
}
