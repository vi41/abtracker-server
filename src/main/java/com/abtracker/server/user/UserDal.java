package com.abtracker.server.user;

import com.abtracker.server.domain.User;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

@Repository
public class UserDal {

    private SessionFactory sessionFactory;

    public UserDal(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public User loadUserByName(String name) {
        return (User) sessionFactory.getCurrentSession().createQuery("FROM User user WHERE user.name = :name")
                .setString("name", name).uniqueResult();
    }

    public void saveUser(User user) {
        sessionFactory.getCurrentSession().save(user);
    }
}
