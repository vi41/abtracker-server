package com.abtracker.server.core;

import com.abtracker.server.domain.SessionData;
import com.abtracker.server.domain.Status;
import com.abtracker.server.domain.User;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.security.oauth2.common.OAuth2RefreshToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.util.UUID;

@Service
public class SessionDataTokenStore extends JdbcTokenStore implements ApplicationEventPublisherAware {

    private SessionDataDal sessionDataDal;

    private ApplicationEventPublisher applicationEventPublisher;

    public SessionDataTokenStore(DataSource dataSource, SessionDataDal sessionDataDal) {
        super(dataSource);
        this.sessionDataDal = sessionDataDal;
    }

    @Override
    @Transactional
    public void storeRefreshToken(OAuth2RefreshToken refreshToken, OAuth2Authentication authentication) {
        UUID userId = ((CurrentUserDetails) authentication.getPrincipal()).getUserId();
        sessionDataDal.removeByUserId(userId);
        SessionData sessionData = new SessionData(new User(userId), Status.OFFLINE);
        sessionDataDal.save(sessionData);
        super.storeRefreshToken(refreshToken, authentication);
    }

    @Override
    @Transactional
    public void removeRefreshToken(OAuth2RefreshToken token) {
        OAuth2Authentication auth = this.readAuthenticationForRefreshToken(token);
        UUID userId = null;
        if (auth != null) {
            userId = ((CurrentUserDetails) auth.getPrincipal()).getUserId();
            sessionDataDal.removeByUserId(userId);

        }
        super.removeRefreshToken(token);
        if (userId != null) {
            this.applicationEventPublisher.publishEvent(new UserSessionRemovedEvent(this, userId));
        }
    }

    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;
    }
}
