package com.abtracker.server.core;

import com.fasterxml.jackson.annotation.JsonProperty;

public class WebSocketMessage {

    @JsonProperty("message_type")
    private String messageType;

    public WebSocketMessage() {
        String messageType = this.getClass().getName();
        messageType = messageType.substring(messageType.lastIndexOf(".") + 1);
        messageType = messageType.replaceAll("([A-Z])", "_$1");
        if(messageType.startsWith("_")){
            messageType = messageType.substring(1);
        }
        this.messageType = messageType.toLowerCase();
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
}
}
