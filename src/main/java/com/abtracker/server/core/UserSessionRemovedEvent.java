package com.abtracker.server.core;

import org.springframework.context.ApplicationEvent;

import java.util.UUID;

public class UserSessionRemovedEvent extends ApplicationEvent {
    private UUID userId;

    public UserSessionRemovedEvent(Object source, UUID userId) {
        super(source);
        this.userId = userId;
    }

    public UUID getUserId() {
        return userId;
    }
}
