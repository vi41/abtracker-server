package com.abtracker.server.core;

import com.abtracker.server.domain.SessionData;
import com.abtracker.server.domain.Status;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public class SessionDataDal {

    private SessionFactory sessionFactory;

    public SessionDataDal(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public int removeByUserId(UUID userId) {
        return sessionFactory.getCurrentSession()
                .createQuery("DELETE FROM SessionData sessionData WHERE sessionData.user.id = :id")
                .setParameter("id", userId).executeUpdate();
    }

    public SessionData loadByUserName(String userName) {
        return (SessionData) sessionFactory.getCurrentSession()
                .createQuery("FROM SessionData sessionData WHERE sessionData.user.name = :userName")
                .setString("userName", userName).uniqueResult();
    }

    public SessionData loadByUserId(UUID id) {
        return (SessionData) sessionFactory.getCurrentSession()
                .createQuery("FROM SessionData sessionData WHERE sessionData.user.id = :id")
                .setParameter("id", id).uniqueResult();
    }

    public int setStatus(UUID userId, Status status) {
        return sessionFactory.getCurrentSession()
                .createQuery("UPDATE SessionData sessionData SET sessionData.status = :status " +
                        "WHERE sessionData.user.id = :id")
                .setParameter("id", userId).setParameter("status", status).executeUpdate();
    }

    public void resetAllStatusesInSeparateSession(){
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.createQuery("UPDATE SessionData sessionData SET sessionData.status = :status ")
                .setParameter("status", Status.OFFLINE).executeUpdate();
        transaction.commit();
        session.close();
    }

    public void save(SessionData sessionData) {
        sessionFactory.getCurrentSession().save(sessionData);
    }

}
