package com.abtracker.server.core;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.UUID;

@Service
public class AuthUtils {

    public UUID getUserId() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return auth != null ? ((CurrentUserDetails) auth.getPrincipal()).getUserId() : null;
    }

    public boolean hasAuthority(OAuth2Authentication auth, String authority) {
        Collection<GrantedAuthority> authorities = auth != null ? auth.getAuthorities() : null;
        return authorities != null && authorities.stream().anyMatch(toCheck ->
                authority.equals(toCheck.getAuthority()));
    }

    public UUID getUserId(Authentication auth){
        return ((CurrentUserDetails)auth.getPrincipal()).getUserId();
    }
}
