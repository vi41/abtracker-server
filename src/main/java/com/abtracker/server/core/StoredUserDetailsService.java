package com.abtracker.server.core;

import com.abtracker.server.domain.User;
import com.abtracker.server.user.UserDal;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.MessageFormat;

@Component
@Qualifier(value = "tokenStore")
public class StoredUserDetailsService implements UserDetailsService {

    private static final Logger logger = Logger.getLogger(StoredUserDetailsService.class);

    private UserDal userDal;

    public StoredUserDetailsService(UserDal userDal) {
        this.userDal = userDal;
    }

    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        try {
            username = URLDecoder.decode(username, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            logger.info("Unable to decode username: " + e);
        }
        User user = userDal.loadUserByName(username);
        if (user != null) {
            return new CurrentUserDetails(user.getId(), username, user.getPassword(),
                    AuthorityUtils.createAuthorityList("USER"));
        } else {
            throw new UsernameNotFoundException(MessageFormat.format("Unable to find user width username - {0}", username));
        }
    }
}
