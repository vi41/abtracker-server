package com.abtracker.server.core;

import com.abtracker.server.domain.Status;
import com.abtracker.server.user.StatusService;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.WebSocketHandlerDecorator;
import org.springframework.web.socket.handler.WebSocketHandlerDecoratorFactory;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;
import org.springframework.web.socket.server.HandshakeInterceptor;

import java.io.IOException;
import java.security.Principal;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Service
public class WebSocketController implements HandshakeInterceptor {

    private final Object sessionLock = new Object();

    private final Map<String, WebSocketSession> sessionById = new HashMap<>();

    private final Map<UUID, WebSocketSession> sessionByUserId = new HashMap<>();

    private static final Logger logger = Logger.getLogger(WebSocketController.class);

    private TokenStore tokenStore;

    private StatusService statusService;

    private AuthUtils authUtils;

    @Bean
    public ApplicationListener<UserSessionRemovedEvent> userSessionRemovedListener() {
        return this::onUserSessionRemoved;
    }

    @Bean
    public ApplicationListener<SessionDisconnectEvent> webSocketDisconnectListener() {
        return this::onWebSocketDisconnect;
    }

    private WebSocketHandlerDecoratorFactory webSocketHandlerDecoratorFactory = new WebSocketHandlerDecoratorFactory() {
        @Override
        public WebSocketHandler decorate(final WebSocketHandler handler) {
            return new WebSocketHandlerDecorator(handler) {
                @Override
                public void afterConnectionEstablished(final WebSocketSession session) throws Exception {
                    WebSocketController.this.register(session);
                    super.afterConnectionEstablished(session);
                }
            };
        }
    };

    public WebSocketController(TokenStore tokenStore, StatusService statusService, AuthUtils authUtils) {
        this.tokenStore = tokenStore;
        this.statusService = statusService;
        this.authUtils = authUtils;
    }

    public WebSocketHandlerDecoratorFactory getWebSocketHandlerDecoratorFactory() {
        return webSocketHandlerDecoratorFactory;
    }

    private boolean hasAuthority(OAuth2Authentication auth) {
        return authUtils.hasAuthority(auth, "USER");
    }

    private void checkAuthorities(OAuth2Authentication auth) {
        if (!hasAuthority(auth)) {
            throw new AccessDeniedException("Access denied");
        }
    }

    private void closeSession(WebSocketSession session) {
        try {
            session.close();
        } catch (IOException exc) {
            logger.error(MessageFormat.format("Unable to close session with id {0}", session.getId()), exc);
        }
    }

    private void setUserSession(UUID userId, WebSocketSession session) {
        WebSocketSession oldSession;
        synchronized (sessionLock) {
            oldSession = sessionByUserId.remove(userId);
            if (oldSession != null) {
                sessionById.remove(oldSession.getId());
            }
            sessionByUserId.put(userId, session);
            sessionById.put(session.getId(), session);
        }
        if (oldSession != null) {
            closeSession(oldSession);
            statusService.setStatus(userId, Status.OFFLINE);
        }
    }

    public void register(WebSocketSession session) {
        Principal principal = session.getPrincipal();
        OAuth2Authentication auth = principal != null && principal instanceof OAuth2Authentication
                ? (OAuth2Authentication) principal : null;
        if (hasAuthority(auth)) {
            setUserSession(authUtils.getUserId(auth), session);
        }
    }

    private void unregister(String sessionId) {
        UUID userId = null;
        synchronized (sessionLock) {
            WebSocketSession session = sessionById.remove(sessionId);
            if (session != null) {
                Principal principal = session.getPrincipal();
                if (principal != null && principal instanceof Authentication) {
                    Authentication auth = (Authentication) principal;
                    CurrentUserDetails userDetails = (CurrentUserDetails) auth.getPrincipal();
                    if (userDetails != null) {
                        userId = userDetails.getUserId();
                        sessionByUserId.remove(userId);
                    }
                }
            }
        }
        if (userId != null) {
            statusService.setStatus(userId, Status.OFFLINE);
        }
    }

    public void onWebSocketDisconnect(SessionDisconnectEvent event) {
        String sessionId = event.getSessionId();
        unregister(sessionId);
    }

    public void onUserSessionRemoved(UserSessionRemovedEvent event) {
        WebSocketSession session;
        synchronized (sessionLock) {
            session = sessionByUserId.get(event.getUserId());
        }
        if (session != null) {
            closeSession(session);
        }
    }

    @Override
    public boolean beforeHandshake(ServerHttpRequest request, ServerHttpResponse response, WebSocketHandler wsHandler,
                                   Map<String, Object> attributes) throws Exception {
        OAuth2Authentication auth = null;
        List<NameValuePair> params = URLEncodedUtils.parse(request.getURI(), "UTF-8");
        if (params != null) {
            for (NameValuePair param : params) {
                if ("accessToken".equals(param.getName())) {
                    OAuth2AccessToken accessToken = tokenStore.readAccessToken(param.getValue());
                    auth = accessToken != null ? tokenStore.readAuthentication(accessToken) : null;
                    break;
                }
            }
        }
        checkAuthorities(auth);
        SecurityContextHolder.getContext().setAuthentication(auth);
        return true;
    }

    @Override
    public void afterHandshake(ServerHttpRequest request, ServerHttpResponse response, WebSocketHandler wsHandler, Exception exception) {
        // do nothing
    }
}
