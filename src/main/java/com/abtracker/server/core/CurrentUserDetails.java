package com.abtracker.server.core;

import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.UUID;


public class CurrentUserDetails extends org.springframework.security.core.userdetails.User {
    private UUID userId = null;

    public CurrentUserDetails(UUID userId, String username, String password, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
        this.userId = userId;
    }

    public UUID getUserId() {
        return userId;
    }
}
