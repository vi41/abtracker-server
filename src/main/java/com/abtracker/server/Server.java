package com.abtracker.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@ImportResource("persistence.xml")
public class Server {
    public static void main(String[] args) {
        SpringApplication.run(Server.class, args);
    }
}