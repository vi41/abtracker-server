package com.abtracker.server.messages;

import com.abtracker.server.core.WebSocketMessage;
import com.abtracker.server.domain.Status;

public class UserStatus extends WebSocketMessage {
    private Status status;

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
