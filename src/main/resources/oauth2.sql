create table oauth_client_details (
  client_id VARCHAR(256) PRIMARY KEY,
  resource_ids VARCHAR(256),
  client_secret VARCHAR(256),
  scope VARCHAR(256),
  authorized_grant_types VARCHAR(256),
  web_server_redirect_uri VARCHAR(256),
  authorities VARCHAR(256),
  access_token_validity INTEGER,
  refresh_token_validity INTEGER,
  additional_information VARCHAR(4096),
  autoapprove VARCHAR(256)
);

create table oauth_access_token (
  token_id VARCHAR(256),
  token bytea,
  authentication_id VARCHAR(256),
  user_name VARCHAR(256),
  client_id VARCHAR(256),
  authentication bytea,
  refresh_token VARCHAR(256),
  UNIQUE (client_id, user_name)
);

create table oauth_code (
  code VARCHAR(256), authentication bytea
);

create table oauth_refresh_token (
  token_id VARCHAR(256),
  token bytea,
  authentication bytea
);

/* Create frontend client with password 'qwerty' (BCrypt password hash). Access token validity: 1 hour,
 refresh token validity 14 days */
insert into oauth_client_details (client_id, resource_ids, client_secret, scope, authorized_grant_types,
 web_server_redirect_uri, authorities, access_token_validity, refresh_token_validity)
values ('abtracker', NULL, '$2a$10$bUZDQnhhx/sL7Ji9nh0oE.DKgGkagp2Tb18AfYRgGj1YKLIQYpU5W', 'read,write,trust',
'password,authorization_code,refresh_token,implicit', '', 'ROLE_USER', 3600, 1209600);

/* Create administrator with password 'qwerty' (BCrypt password hash) */
insert into users (id, name, password) values ('81253e6c-6c83-4f34-9c2e-7d557b7703d9', 'admin', '$2a$10$bUZDQnhhx/sL7Ji9nh0oE.DKgGkagp2Tb18AfYRgGj1YKLIQYpU5W');